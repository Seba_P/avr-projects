/*
 * Stopwatch.c
 *
 * Created: 2015-07-20 11:55:39
 *  Author: SaddamAZR
 */ 

#include <stdbool.h>
#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "hd44780.h"
#include "defines.h"

//----------DEKLARACJE ZMIENNYCH GLOBALNYCH----------//
volatile uint32_t time; //aktualny czas stopera
extern char text_line1[17]; //pierwsza linia LCD
extern char text_line2[17]; //druga linia LCD
uint8_t players_set,players,switch_pressed; //rejestry do ustawiania zawodnikow
char time_s2[9],time_s3[9],time_s4[9],time_s5[9],time_s6[9],time_s7[9],time_s8[9];// ----------------> kupa
char** times_p[7]={time_s2,time_s3,time_s4,time_s5,time_s6,time_s7,time_s8}; //wskazniki na czasy psozczegolnych zawodnikow
extern char time_buffer[9]; //bufor stringow
char order_s[9];
uint8_t order[7],i; //kolejnosc zawodnikow,nr zawodnika na mecie


//----------DEKLARACJE FUNKCJI----------//
void timer1_init();
void settings();
void check_switches();
void show_times();
void convert(volatile uint32_t);
void choose_player_time();
void run();
void finish();
void copy_str(char*,volatile char*);

void lcd_init();
void lcd_puttext(const char __memx *);
void lcd_goto(uint8_t, uint8_t);

//----------GLOWNY PROGRAM----------//
int main(void)
{
	PORTC=0xFF; //PORTC - wejscie (S1 - start, S5:8 - zawodnicy)
	settings();
	run();
    while(1){}
}
// **************************OK

//----------DEFINICJE FUNKCJI----------//
void timer1_init(void){
	TCCR1B=(1<<WGM12)|(1<<CS10)|(1<<CS11); //TIMER1 - preskaler ustawiony na 1/64*CLK
	uint16_t n=2500; //F_CPU/64 ~~10ms
	TCNT1H=0;
	TCNT1L=0; //TIMER1 - zerowanie licznika
	OCR1AH=(n >> 8);
	OCR1AL=(n & 0x00FF); //TIMER1 - ustawienie rejestru OCR1A tak, aby okres by� r�wny 10ms
	TIMSK1=(1<<OCIE1A); //TIMER1 - aktywacja przerwania
}
// **************************OK
void settings(){
	lcd_init(); //wlaczenie LCD										
	sprintf(text_line1,"***STOPWATCH*** ");
	sprintf(text_line2,"Players:        "); //ustawienie menu startowego
	lcd_goto(0,0);
	lcd_puttext(text_line1);
	lcd_goto(0,1);
	lcd_puttext(text_line2);
	
	players=0;
	switch_pressed=0;
	while((PINC & (1<<PINC0))!=0){ //czekanie na wcisniecie S1
		check_switches(); //ustawianie graczy i przyporzadkowanie ich do przyciskow S2:8
		lcd_goto(0,0);
		lcd_puttext(text_line1);
		lcd_goto(0,1); //piszemy w drugiej linii ------------------------------------------jbc to mozna wstawic rejestr dla zmian players
		lcd_puttext(text_line2);
	} 
	
	players_set=players;
	time=0; //wyzerowanie czasu
	sei(); //wlaczenie przerwan
	timer1_init(); //wlaczenie odliczania
}
// **************************OK
ISR(TIMER1_COMPA_vect){
	time++;
	if(players==0){
		TCCR0B=0; //wylaczenie timera
		show_times();
	}
}
// **************************OK
void check_switches(){ 
	//S2
	if(((switch_pressed & (1<<S2))==0) & ((PINC & (1<<PINC1))==0)){
		switch_pressed |= (1<<S2); //przycisk wcisniety
		players ^= (1<<S2); //ustawienie/usuniecie gracza na przycisku S2
		if((players & (1<<S2))!=0){
			text_line2[9]=0x32; //plamka
		} else{
			text_line2[9]=0x20; //spacja
		}
	} else if(((switch_pressed & (1<<S2))!=0) & ((PINC & (1<<PINC1))!=0)){
		switch_pressed &= ~(1<<S2); //zwolnienie przycisku
	}
	//S3
	if(((switch_pressed & (1<<S3))==0) & ((PINC & (1<<PINC2))==0)){
		switch_pressed |= (1<<S3); //przycisk wcisniety
		players ^= (1<<S3); //ustawienie/usuniecie gracza na przycisku S3
		if((players & (1<<S3))!=0){
			text_line2[10]=0x33; //plamka
			} else{
			text_line2[10]=0x20; //spacja
		}
	} else if(((switch_pressed & (1<<S3))!=0) & ((PINC & (1<<PINC2))!=0)){
		switch_pressed &= ~(1<<S3); //zwolnienie przycisku
	}
	//S4
	if(((switch_pressed & (1<<S4))==0) & ((PINC & (1<<PINC3))==0)){
		switch_pressed |= (1<<S4); //przycisk wcisniety
		players ^= (1<<S4); //ustawienie/usuniecie gracza na przycisku S4
		if((players & (1<<S4))!=0){
			text_line2[11]=0x34; //plamka
			} else{
			text_line2[11]=0x20; //spacja
		}
	} else if(((switch_pressed & (1<<S4))!=0) & ((PINC & (1<<PINC3))!=0)){
		switch_pressed &= ~(1<<S4); //zwolnienie przycisku
	}
	//S5
	if(((switch_pressed & (1<<S5))==0) & ((PINC & (1<<PINC4))==0)){
		switch_pressed |= (1<<S5); //przycisk wcisniety
		players ^= (1<<S5); //ustawienie/usuniecie gracza na przycisku S5
		if((players & (1<<S5))!=0){
			text_line2[12]=0x35; //plamka
			} else{
			text_line2[12]=0x20; //spacja
		}
	} else if(((switch_pressed & (1<<S5))!=0) & ((PINC & (1<<PINC4))!=0)){
		switch_pressed &= ~(1<<S5); //zwolnienie przycisku
	}
	//S6
	if(((switch_pressed & (1<<S6))==0) & ((PINC & (1<<PINC5))==0)){
		switch_pressed |= (1<<S6); //przycisk wcisniety
		players ^= (1<<S6); //ustawienie/usuniecie gracza na przycisku S6
		if((players & (1<<S6))!=0){
			text_line2[13]=0x36; //plamka
			} else{
			text_line2[13]=0x20; //spacja
		}
	} else if(((switch_pressed & (1<<S6))!=0) & ((PINC & (1<<PINC5))!=0)){
		switch_pressed &= ~(1<<S6); //zwolnienie przycisku
	}
	//S7
	if(((switch_pressed & (1<<S7))==0) & ((PINC & (1<<PINC6))==0)){
		switch_pressed |= (1<<S7); //przycisk wcisniety
		players ^= (1<<S7); //ustawienie/usuniecie gracza na przycisku S7
		if((players & (1<<S7))!=0){
			text_line2[14]=0x37; //plamka
			} else{
			text_line2[14]=0x20; //spacja
		}
	} else if(((switch_pressed & (1<<S7))!=0) & ((PINC & (1<<PINC6))!=0)){
		switch_pressed &= ~(1<<S7); //zwolnienie przycisku
	}
	//S8
	if(((switch_pressed & (1<<S8))==0) & ((PINC & (1<<PINC7))==0)){
		switch_pressed |= (1<<S8); //przycisk wcisniety
		players ^= (1<<S8); //ustawienie/usuniecie gracza na przycisku S8
		if((players & (1<<S8))!=0){
			text_line2[15]=0x38; //plamka
			} else{
			text_line2[15]=0x20; //spacja
		}
	} else if(((switch_pressed & (1<<S8))!=0) & ((PINC & (1<<PINC7))!=0)){
		switch_pressed &= ~(1<<S8); //zwolnienie przycisku
	}
}
// **************************OK
void show_times(){
	volatile uint8_t k;
	sprintf(order_s,"       ");
	for(k=0; k<i; k++){
		order_s[k]=order[k]+0x30; //usatwienie odpowiednich znakow w order_s
	}
	sprintf(text_line1,"Order:   %s",order_s); //ustawienie pierwszej linii wyswietlania -------------------------------->tu jest zle
	lcd_goto(0,0);
	lcd_puttext(text_line1);
	switch_pressed=order[0]-1; // w tej funkcji switch_pressed sluzy za przechowanie informacji o ostatnio wcisnietym przycisku
	while(1){
		choose_player_time();
		sprintf(text_line2,"S%c:     %s",switch_pressed+1+0x30,times_p[switch_pressed-1]); //------------tu ogarnac
		lcd_goto(0,1);
		lcd_puttext(text_line2);
	}
}
// **************************OK
void convert(volatile uint32_t t){
	volatile char time_s[9];
	volatile uint8_t time_d;
	time_s[2]=time_s[5]=':';
	time_s[8]='\0'; //znak konca stringu
	time_d = t%10; //cyfra dziesiatek milisekund
	time_s[7]='0'+time_d;
	time_d = (t%100-t%10)/10; //cyfra setek milisekund
	time_s[6]='0'+time_d;
	time_d = (t%1000-t%100)/100; //cyfra jednosci sekund
	time_s[4]='0'+time_d;
	time_d = (t%6000-t%1000)/1000; //cyfra dziesiatek sekund
	time_s[3]='0'+time_d;
	time_d = (t%60000-t%6000)/6000; //cyfra jednosci minut
	time_s[1]='0'+time_d;
	time_d = (t%600000-t%60000)/60000; //cyfra dziesiatek minut
	time_s[0]='0'+time_d;
	
	copy_str(time_buffer,time_s);	
}
// **************************OK
void choose_player_time(){ // w tej funkcji switch_pressed sluzy za przechowanie informacji o ostatnio wcisnietym przycisku
	//S2
	if(((players_set & (1<<S2))!=0) & ((PINC & (1<<PINC1))==0)){
		switch_pressed=S2; //przycisk wcisniety
	}
	//S3
	if(((players_set & (1<<S3))!=0) & ((PINC & (1<<PINC2))==0)){
		switch_pressed=S3; //przycisk wcisniety
	}
	//S4
	if(((players_set & (1<<S4))!=0) & ((PINC & (1<<PINC3))==0)){
		switch_pressed=S4; //przycisk wcisniety
	}
	//S5
	if(((players_set & (1<<S5))!=0) & ((PINC & (1<<PINC4))==0)){
		switch_pressed=S5; //przycisk wcisniety
	}
	//S6
	if(((players_set & (1<<S6))!=0) & ((PINC & (1<<PINC5))==0)){
		switch_pressed=S6; //przycisk wcisniety
	}
	//S7
	if(((players_set & (1<<S7))!=0) & ((PINC & (1<<PINC6))==0)){
		switch_pressed=S7; //przycisk wcisniety
	}
	//S8
	if(((players_set & (1<<S8))!=0) & ((PINC & (1<<PINC7))==0)){
		switch_pressed=S8; //przycisk wcisniety
	}
}
// **************************OK
void run(){
	sprintf(time_buffer,"00:00:00"); //inicjalizacja time_buffera
	i=0; //pozycja gracza na mecie
	sprintf(order_s,"        ");
	sprintf(text_line1,"Time:           ");
	lcd_goto(0,0);
	lcd_puttext(text_line1);
	
	while(players!=0){
		convert(time);
		finish();
		lcd_goto(8,0);
		lcd_puttext(time_buffer);
		lcd_goto(0,1);
		lcd_puttext(text_line2);
	}
}
// **************************OK
void finish(){
	//S2
	if(((players & (1<<S2))!=0) & ((PINC & (1<<PINC1))==0)){
		players &= ~(1<<S2); //usuniecie gracza na przycisku S2
		text_line2[9]=0x20; //usuniecie gracza z wyswietlacza
		copy_str(time_s2,time_buffer);
		order[i]=S2+1; //ustawiamy kolejnosc na mecie
		i++;
	}
	//S3
	if(((players & (1<<S3))!=0) & ((PINC & (1<<PINC2))==0)){
		players &= ~(1<<S3); //usuniecie gracza na przycisku S3
		text_line2[10]=0x20; //usuniecie gracza z wyswietlacza
		copy_str(time_s3,time_buffer);
		order[i]=S3+1; //ustawiamy kolejnosc na mecie
		i++;
	}
	//S4
	if(((players & (1<<S4))!=0) & ((PINC & (1<<PINC3))==0)){
		players &= ~(1<<S4); //usuniecie gracza na przycisku S4
		text_line2[11]=0x20; //usuniecie gracza z wyswietlacza
		copy_str(time_s4,time_buffer);
		order[i]=S4+1; //ustawiamy kolejnosc na mecie
		i++;
	}
	//S5
	if(((players & (1<<S5))!=0) & ((PINC & (1<<PINC4))==0)){
		players &= ~(1<<S5); //usuniecie gracza na przycisku S5
		text_line2[12]=0x20; //usuniecie gracza z wyswietlacza
		copy_str(time_s5,time_buffer);
		order[i]=S5+1; //ustawiamy kolejnosc na mecie
		i++;
	}
	//S6
	if(((players & (1<<S6))!=0) & ((PINC & (1<<PINC5))==0)){
		players &= ~(1<<S6); //usuniecie gracza na przycisku S6
		text_line2[13]=0x20; //usuniecie gracza z wyswietlacza
		copy_str(time_s6,time_buffer);
		order[i]=S6+1; //ustawiamy kolejnosc na mecie
		i++;
	}
	//S7
	if(((players & (1<<S7))!=0) & ((PINC & (1<<PINC6))==0)){
		players &= ~(1<<S7); //usuniecie gracza na przycisku S7
		text_line2[14]=0x20; //usuniecie gracza z wyswietlacza
		copy_str(time_s7,time_buffer);
		order[i]=S7+1; //ustawiamy kolejnosc na mecie
		i++;
	}
	//S8
	if(((players & (1<<S8))!=0) & ((PINC & (1<<PINC7))==0)){
		players &= ~(1<<S8); //usuniecie gracza na przycisku S8
		text_line2[15]=0x20; //usuniecie gracza z wyswietlacza
		copy_str(time_s8,time_buffer);
		order[i]=S8+1; //ustawiamy kolejnosc na mecie
		i++;
	}
} 
// **************************OK
void copy_str(char* dest,volatile char* source){
	uint8_t k=0;
	while(*(source+k)!='\0'){
		*(dest+k)=*(source+k);
		k++;
	}
}
// **************************OK
