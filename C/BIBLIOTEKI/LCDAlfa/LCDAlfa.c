/*
 * LCDAlfaInit.c
 *
 * Created: 2014-08-15 11:03:19
 *  Author: tmf
 */ 


#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "HD44780.h"
#include <avr/pgmspace.h>
#include <util/delay.h>

/*Zmienne wykorzystywane do przechowywania stringow:
	text_line1[] - string dla wiersza 0 (na gorze);
	text_line2[] - string dla wiersza 1 (na dole);
	timebuffer[] - string przechowujacy czas; 
*/
char text_line1[17],text_line2[17],time_buffer[9];

/*Inicjalizacja LCD*/
void LCD_init(){
	HD44780_init();				//Podstawowa inicjalizacja modu�u
	HD44780_outcmd(HD44780_CLR);	//Wyczy�� pami�� DDRAM
	HD44780_wait_ready(1000);
	HD44780_outcmd(HD44780_ENTMODE(1, 0));	//Tryb autoinkrementacji AC
	HD44780_wait_ready(1000);
	HD44780_outcmd(HD44780_DISPCTL(1, 0, 0));	//W��cz wy�wietlacz, wy��cz kursor
	HD44780_wait_ready(1000);
}

/*Wyslanie na LCD pojedynczego znaku:
	c - znak do wyslania;
*/
void LCD_putchar(char c){
	static bool second_nl_seen;
	static uint8_t line=0;
	
	if ((second_nl_seen) && (c != '\n')&&(line==0))
	{//Odebrano pierwszy znak
		HD44780_wait_ready(40);
		HD44780_outcmd(HD44780_CLR);
		HD44780_wait_ready(1600);
		second_nl_seen=false;
	}
	if (c == '\n')
	{
		if (line==0)
		{
			line++;
			HD44780_outcmd(HD44780_DDADDR(64));	//Adres pierwszego znaku drugiej linii
			HD44780_wait_ready(1000);
		}
		else
		{
			second_nl_seen=true;
			line=0;
		}
	}
	else
	{
		HD44780_outdata(c);
		HD44780_wait_ready(40);
	}
}

/*Wyslanie na LCD stringu umieszczonego w pamieci FLASH:
	*txt - wskaznik na adres stringu w pamieci FLASH;
*/
void LCD_puttext_P(const char __flash *txt){
	char ch;
	while((ch=*txt))
	{
		LCD_putchar(ch);
		txt++;
	}
}

/*Wyslanie na LCD stringu umieszczonego w pamieci programu:
	*txt - wskaznik na adres stringu w pamieci programu;
*/
void LCD_puttext(const char __memx *txt){
	char ch;
	while((ch=*txt))
	{
		LCD_putchar(ch);
		txt++;
	}
}

/*Ustawienie kursora w odpowiednim miejscu:
	sign_nr - nr znaku (numerowany od 0);
	line_nr - nr wiersza (numerowany od zera);
*/
void LCD_goto(uint8_t sign_nr, uint8_t line_nr){
	HD44780_outcmd(HD44780_DDADDR(0x40*line_nr+sign_nr));
	HD44780_wait_ready(1000);
}

/*Wyczyszczenie zawartosci pamieci wyswietlacza*/
void LCD_cls(){
	HD44780_outcmd(HD44780_CLR);
	HD44780_wait_ready(false);
}

/*Definicja nowego znaku w pamieci CGRAM:
	charno - nr znaku;
	*chardef - wskaznik na adres w pamieci programu zawierajacy odpowiednie wartosci;
*/
void LCD_defchar(uint8_t charno, const uint8_t __memx *chardef){
	HD44780_outcmd(HD44780_CGADDR(charno*8));
	HD44780_wait_ready(40);
	
	for(uint8_t c=0; c<8; c++){
		HD44780_outdata(*chardef++);
		HD44780_wait_ready(40);
	}
}