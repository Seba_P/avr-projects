/*******************************************************************/
/* Obsluga wyswietlacza LCD alfanumerycznego z kontrolerem HD44780 */
/*																   */
/* Autor: Tomasz Francuz & Sebastian Pisklak					   */
/*******************************************************************/

#ifndef LCDAlfa_GUARD
	#define LCDAlfa_GUARD
/*Zmienne wykorzystywane do przechowywania stringow:
	text_line1[] - string dla wiersza 0 (na gorze);
	text_line2[] - string dla wiersza 1 (na dole);
	timebuffer[] - string przechowujacy czas; 
*/
	extern char text_line1[17],text_line2[17],time_buffer[9];
	
/*Inicjalizacja LCD*/
	void LCD_init();
	
/*Wyslanie na LCD pojedynczego znaku:
	c - znak do wyslania;
*/
	void LCD_putchar(char c);
	
/*Wyslanie na LCD stringu umieszczonego w pamieci FLASH:
	*txt - wskaznik na adres stringu w pamieci FLASH;
*/
	void LCD_puttext_P(const char __flash *txt);
	
/*Wyslanie na LCD stringu umieszczonego w pamieci programu:
	*txt - wskaznik na adres stringu w pamieci programu;
*/
	void LCD_puttext(const char __memx *txt);
	
/*Ustawienie kursora w odpowiednim miejscu:
	sign_nr - nr znaku (numerowany od 0);
	line_nr - nr wiersza (numerowany od zera);
*/
	void LCD_goto(uint8_t sign_nr, uint8_t line_nr);

/*Wyczyszczenie zawartosci pamieci wyswietlacza*/
	void LCD_cls();

/*Definicja nowego znaku w pamieci CGRAM:
	charno - nr znaku;
	*chardef - wskaznik na adres w pamieci programu zawierajacy odpowiednie wartosci;
*/
	void LCD_defchar(uint8_t charno, const uint8_t __memx *chardef);
#endif