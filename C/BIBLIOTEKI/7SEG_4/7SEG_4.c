#include "SSEG_4.h"
#include "defines.h"
#include <avr/io.h>

/*Obsluga wyswietlacza siedmiosegmentowego*/
void 7SEG_4_display(uint16_t counter, uint8_t *data_7seg){
	uint8_t disp_digit;
	//I cyfra
	PORT_DIGIT=~(1 << FIRST_DIGIT_PIN); // wybor 1 cyfry
	disp_digit=(counter & 0x000F);
	disp_digit=*(data_7seg+disp_digit);
	PORT_DATA=~(disp_digit); //negacja, bo stan wysoki dla "0"
	_delay_ms(50);
	//II cyfra
	PORT_DIGIT << 1; // wybor 2 cyfry
	disp_digit=((counter & 0x00F0)>>4);
	disp_digit=*(data_7seg+disp_digit);
	PORT_DATA=~(disp_digit);
	_delay_ms(50);
	//III cyfra
	PORT_DIGIT << 1; // wybor 3 cyfry
	disp_digit=((counter & 0x0F00)>>8);
	disp_digit=*(data_7seg+disp_digit);
	PORT_DATA=~(disp_digit);
	_delay_ms(50);
	//IV cyfra
	PORT_DIGIT << 1; // wybor 4 cyfry
	disp_digit=((counter & 0xF000)>>12);
	disp_digit=*(data_7seg+disp_digit);
	PORT_DATA=~(disp_digit);
	_delay_ms(50);
}