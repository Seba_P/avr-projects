/*****************************************************************************************/
/* Obsluga wyswietlacza siedmiosegmentowego - 4 cyfry, wspolna katoda, '0' - stan wysoki */
/*																						 */
/* Autor: Sebastian Pisklak																 */
/*****************************************************************************************/
#ifndef SSEG_4_GUARD
	#define SSEG_4_GUARD
	
	/*PORT_DIGIT - port IO, do ktorego podlaczona jest katoda wyswietlacza*/
	#define PORT_DIGIT PORTA
	/*FIRST_DIGIT_PIN - nr pinu, do ktorego podlaczona jest pierwsza cyfra wyswietlacza (od lewej); UWAGA! wszystkie cyfry musza byc podlaczone do pinow po kolei co 1 poczawszy od pierwszej*/
	#define FIRST_DIGIT_PIN 0
	/*PORT_DATA - port IO, na ktory beda wysylane dane sterujace segmentami*/
	#define PORT_DATA  PORTB

	/*Funkcja obslugujaca wyswietlacz siedmosegmentowy*/
	void 7SEG_4_display(uint16_t counter, uint8_t *data_7seg);
#endif