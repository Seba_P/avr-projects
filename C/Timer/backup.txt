/*
 * Timer.c
 *
 * Created: 2015-07-02 12:16:54
 *  Author: Seba
 */ 
#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

//----------DEKLARACJA ZMIENNYCH GLOBALNYCH----------//
uint16_t counter=0; //rejestr licznika dla naszego minutnika
uint8_t data_7seg[16]={0x7e, 0x30, 0x6d, 0x79, 0x33, 0x5b, 0x5f, 0x70, 0x7f, 0x7b, 0x77, 0x1f, 0x4e, 0x3d, 0x4f, 0x47};
uint8_t button_1h,button_1l,button_2h,button_2l; //buttons - PC7:4
uint8_t disp_digit,disp_reg;

//----------DEKLARACJA FUNKCJI----------//
void start(void);
void display(void);
void timer1_init(void);

//----------G��WNY PROGRAM----------//
int main(void){
	DDRA=0xFF; //PORTA - wyjscie (wybor cyfry 7SEG)
	DDRB=0xFF; //PORTB - wyjscie (dane na 7SEG)
	PORTC=0xFF; //PORTC - wejscie (PC7:4 - regulacja czasu, PC0 - guzik startu); pull'ups dla switchy 	
	DDRD=0xFF; //PORTD - wyjscie (LED)
	
	start();
	//program:
	sei(); // w tym miejscu w celu unikniecia bledu wyjscia z ISR
	while(1){
		display();
	}
	return 0;
}
//----------KONSTRUKCJA FUNKCJI----------//
void start(void){
	PORTD=0xFE; //ustawianie diod - swieci tylko dioda na PD0 - sygnal oczekiwania
	counter=0;
	button_1h=button_1l=button_2h=button_2l=0;
	
	while(PC0==1){
		//IV cyfra
		if((button_2h==0) & (PC4==0)){
			button_2h=1; //wykrycie wcisnietego przycisku
			counter+=0x1000; //inkrementacja 4 cyfry licznika
			} else if((button_2h==1) & (PC4==1)){
			button_2h=0; //zwolnienie przycisku
		}
		//III cyfra
		if((button_2l==0) & (PC5==0)){
			button_2l=1; //wykrycie wcisnietego przycisku
			counter+=0x0100; //inkrementacja 3 cyfry licznika
			} else if((button_2l==1) & (PC5==1)){
			button_2l=0; //zwolnienie przycisku
		}
		//II cyfra
		if((button_1h==0) & (PC6==0)){
			button_1h=1; //wykrycie wcisnietego przycisku
			counter+=0x0010; //inkrementacja 2 cyfry licznika
			} else if((button_1h==1) & (PC6==1)){
			button_1h=0; //zwolnienie przycisku
		}
		//I cyfra
		if((button_1l==0) & (PC7==0)){
			button_1l=1; //wykrycie wcisnietego przycisku
			counter+=0x0001; //inkrementacja 1 cyfry licznika
			} else if((button_1l==1) & (PC7==1)){
			button_1l=0; //zwolnienie przycisku
		}
		
		display();
	}
	PORTD=0xFF; //zgaszenie diod - sygnal startu
	timer1_init();
}
void display(void){
	//I cyfra
	PORTA=~(0x01); // wybor 1 cyfry
	disp_digit=(counter & 0x000F);
	disp_reg=*(data_7seg+disp_digit);
	PORTB=~(disp_reg); //negacja, bo stan wysoki dla "0"
	_delay_ms(50);
	//II cyfra
	PORTA=~(0x02); // wybor 2 cyfry
	disp_digit=((counter & 0x00F0)>>4);
	disp_reg=*(data_7seg+disp_digit);
	PORTB=~(disp_reg);
	_delay_ms(50);
	//III cyfra
	PORTA=~(0x04); // wybor 3 cyfry
	disp_digit=((counter & 0x0F00)>>8);
	disp_reg=*(data_7seg+disp_digit);
	PORTB=~(disp_reg);
	_delay_ms(50);
	//IV cyfra
	PORTA=~(0x08); // wybor 4 cyfry
	disp_digit=((counter & 0xF000)>>12);
	disp_reg=*(data_7seg+disp_digit);
	PORTB=~(disp_reg);
	_delay_ms(50);
}
void timer1_init(void){
	TCCR1B=(1<<WGM12)|(1<<CS10)|(1<<CS12); //TIMER1 - preskaler ustawiony na 1/1024*CLK
	TCCR1A=(1<<COM1A0); //TIMER1 - tryb CTC
	uint16_t n=977;
	//uint8_t high,low;
	//high=(n>>8);
	//low=n&0x00ff;
	TCNT1H=0;
	TCNT1L=0; //TIMER1 - zerowanie licznika
	OCR1AH=(n >> 8);
	OCR1AL=(n & 0x00FF); //TIMER1 - ustawienie rejestru OCR1A tak, aby okres by� r�wny 1s
	TIMSK1=(1<<OCIE1A); //TIMER1 - aktywacja przerwania
}
ISR(TIMER1_COMPA_vect){
	counter--;
	if(counter == 0){
		TCCR1B=0; //TIMER1 - wylaczenie
		start();
		//goto program;
	}
}