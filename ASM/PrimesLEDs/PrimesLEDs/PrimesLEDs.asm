/*
 * PrimesLEDs.asm
 *
 *  Created: 2015-04-30 22:15:55
 *   Author: SaddamAZR
 */ 
 .DEF counter1=r20; inicjalizacja licznikow i wartosci
 .DEF counter2=r21;
 .DEF counter3=r22;
 .EQU value1=248;
 .EQU value2=200;
 .EQU value3=107;

 .CSEG
 .ORG 0x00;
 rjmp start;

 .ORG 0x32;
 prime_cseg: .DB 2,3,5,7,11,13,17,19,23,29; 10 poczatkowych liczb pierwszych
 
 .DSEG
 .ORG 0x100;
 prime_dseg: .BYTE 10; rezerwacja miejsca w pamieci danych

 .CSEG
 .ORG 0x100; start pamieci programu
 start: 
 ldi r16,0xff;
 out ddrb,r16; PORTB - wyjscie (LED)
 ldi zh,high(2*prime_cseg); ladowanie do rejestru X adresu danych z prime:
 ldi zl,low(2*prime_cseg);
 ldi yh,high(prime_dseg);
 ldi yl,low(prime_dseg);
 ldi r17,10;
 store_to_dseg:; przepisanie zawartosci prime_cseg: do prime_dseg:
 lpm r16,z+;
 st y+,r16;
 dec r17;
 brne store_to_dseg;

 reset:
 ldi yh,high(prime_dseg); reset wskaznika
 ldi yl,low(prime_dseg);
 program:
 ld r16,y+;
 com r16;
 out portb,r16; wyswietlanie na LED'ach
 com r16;
 call one_sec_delay;
 cpi r16,29;
 breq reset;
 rjmp program;

 one_sec_delay:; procedura opozniajaca
 loop1:
 ldi counter1,value1;
 ldi counter2,value2;
 ldi counter3,value3;
 loop2:
 dec counter1;
 brne loop2;
 ldi counter1,value1;
 dec counter2;
 brne loop2;
 ldi counter1,value1;
 ldi counter2,value2
 dec counter3;
 brne loop2
 ret
