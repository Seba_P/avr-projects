/*
 * timer1_1sec.asm
 *
 *  Created: 2015-06-08 22:14:22
 *   Author: SaddamAZR
 */ 

 ldi r16,(1<<pd5);
 out ddrd,r16; PD5 - wyjscie (LED)
 ldi r16,5|(1<<wgm12);
 sts tccr1b,r16; TIMER1 - 1/1024MHz
 ldi r16,(1<<com1a0);
 sts tccr1a,r16; TIMER1 - tryb CTC
 ldi r16,high(16*977);
 sts ocr1ah,r16;
 ldi r16,0x00;
 sts ocr1al,r16;

 start:
 rjmp start;
