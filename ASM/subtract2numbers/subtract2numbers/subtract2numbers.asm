/*
 * subtract2numbers.asm
 *
 *  Created: 2015-05-14 19:53:05
 *   Author: SaddamAZR
 */ 
 .DEF num1_high=r20; deklaracja  rejestrow do odejmowania
 .DEF num1_low=r21;
 .DEF num2_high=r22;
 .DEF num2_low=r23;
 .DEF addr_z=r24;
 .DEF disp_reg=r25;
 .DEF counter1=r17;
 .DEF counter2=r18;
 .EQU value1=80;
 .EQU value2=200;
 .CSEG
 .ORG 0x00 
 rjmp start;

 .ORG 0x32 
 data_7seg: .DB 0x7e, 0x30, 0x6d, 0x79, 0x33, 0x5b, 0x5f, 0x70, 0x7f, 0x7b, 0x77, 0x1f, 0x4e, 0x3d, 0x4f, 0x47;  

 .ORG 0x100;
 start:
 ldi r16,high(RAMEND); inicjalizacja stosu
 out sph,r16;
 ldi r16,low(RAMEND);
 out spl,r16;

 ldi addr_z,low(2*data_7seg); dolny bajt adresu data_7seg:
 ldi zl,low(2*data_7seg);
 ldi zh,high(2*data_7seg); inicjalizacja wskaznika Z
 ldi r16,0xff;
 out ddrb,r16; PORTB - wyjscie (7SEG)
 out ddra,r16; PORTA - wyjscie (cyfra 7SEG)
 out ddrd,r16; PORTD - wyjscie (diody dla flag S (bit 6) i V (bit 7))
 out portd,r16; gaszenie wszystkich diod

 ldi num1_high,192; num1 = 512+240=752
 ldi num1_low,24;
 ldi num2_high,127; num2 = 2560+63=2623
 ldi num2_low,255;
 sub num1_low,num2_low;
 sbc num1_high,num2_high;

 program:
 brlt S;
 rjmp no_S;
 S: 
 cbi portd,6;
 no_S:
 brvs V;
 rjmp no_V;
 V: 
 cbi portd,7;
 no_V:
 disp:
 rcall display;
 rjmp disp;

 delay:; procedura opozniajaca (~50ms)
 loop:
 dec counter1;
 brne loop;
 ldi counter1,value1;
 dec counter2;
 brne loop;
 ldi counter1,value1;
 ldi counter2,value2;
 ret

 display:; procedura wyswietlania zawartosci licznika na 7SEG
 ; I cyfra
 ldi r16,7;
 out porta,r16;
 mov disp_reg,num1_low;
 andi disp_reg,0x0F;
 add disp_reg,addr_z;
 mov zl,disp_reg;
 lpm r16,z;
 com r16;
 out portb,r16;
 rcall delay;
 ; II cyfra
 ldi r16,11;
 out porta,r16;
 mov disp_reg,num1_low;
 andi disp_reg,0xF0;
 swap disp_reg;
 add disp_reg,addr_z;
 mov zl,disp_reg;
 lpm r16,z;
 com r16;
 out portb,r16;
 rcall delay;
 ; III cyfra
 ldi r16,13;
 out porta,r16;
 mov disp_reg,num1_high;
 andi disp_reg,0x0F;
 add disp_reg,addr_z;
 mov zl,disp_reg;
 lpm r16,z;
 com r16;
 out portb,r16;
 rcall delay;
 ; IV cyfra
 ldi r16,14;
 out porta,r16;
 mov disp_reg,num1_high;
 andi disp_reg,0xF0;
 swap disp_reg;
 add disp_reg,addr_z;
 mov zl,disp_reg;
 lpm r16,z;
 com r16;
 out portb,r16;
 rcall delay;
 ret


