/*
 * timer1_1sec_int.asm
 *
 *  Created: 2015-06-10 22:32:23
 *   Author: SaddamAZR
 */ 
 .ORG 0x00;
 rjmp start;
 .ORG 0x1A;
 rjmp ocf1a_int; interrupt handler
 
 .ORG 0x60;
 start:
 ldi r16,(1<<pa0);
 out ddra,r16; PA0 - wyjscie (LED)
 ldi r16,(1<<cs10)|(1<<cs12)|(1<<wgm12);
 sts tccr1b,r16; TIMER1 - 1/1024MHz
 ldi r16,(1<<com1a0);
 sts tccr1a,r16; TIMER1 - tryb CTC
 ldi r16,high(16*977); zegar 16*
 sts ocr1ah,r16;
 ldi r16,0x00;
 sts ocr1al,r16;
 ldi r16,(1<<ocie1a);
 sts timsk1,r16; wlaczenie przerwania dla TIMER1

 ldi r17,0x01; do XOR
 sei; wlaczenie przerwan
 program:
 rjmp program;

 ocf1a_int:
 in r16,porta;
 eor r16,r17; bit toggle
 out porta,r16;
 reti;

