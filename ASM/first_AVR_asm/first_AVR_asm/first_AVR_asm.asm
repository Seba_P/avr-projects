/*
 * first_AVR_asm.asm
 *
 *  Created: 2015-04-27 12:48:47
 *   Author: SaddamAZR
 */ 

 .EQU decimal=100;
 .EQU hex=0x64;			100
 .EQU bin=0b01100100;	100
 .EQU ascii='S';
  
  ldi r16,decimal;
  ldi r17,hex;
  ldi r18,bin;
  ldi r19,ascii;
  
  stop: rjmp stop;