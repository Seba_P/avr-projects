/*
 * led_dimmer.asm
 *
 *  Created: 2015-06-09 18:47:36
 *   Author: SaddamAZR
 */ 
 .DEF duty_cycle=r17;

 ldi r16,(1<<pa2)|(1<<pa3);
 out porta,r16; PORTA - wejscie (PA2 - "+", PA3 - "-"); podciagniete pull'upy
 ldi r16,(1<<pb3);
 out ddrb,r16; PB3 (OC0A) - wyjscie (LED)
 ldi r16,(1<<cs00)|(1<<cs02);
 out tccr0b,r16; TIMER0 - 1/1024 MHz
 ldi r16,(1<<wgm01)|(1<<wgm00)|(1<<com0a1)|(1<<com0a0);
 out tccr0a,r16; TIMER0 - Fast PWM (non-inv)

 ldi duty_cycle,1; ustawienie jasnosci w polowie
 out ocr0a,duty_cycle;

 program:
 in r16,pina;
 sbrs r16,pa2;
 rjmp plus;
 next:
 sbrs r16,pa3;
 rjmp minus;
 next2:
 out ocr0a,duty_cycle;
 rjmp program;

 plus:
 cpi duty_cycle,241;
 breq plus2; zabezpieczenie przed przepelnieniem duty_cycle
 subi duty_cycle,-16; duty_cycle + 16
 plus2:
 in r16,pina;
 sbrs r16,pa2;
 rjmp plus2;
 rjmp next;
 
 minus:
 cpi duty_cycle,1;
 breq minus2; zabezpieczenie przed przepelnieniem duty_cycle
 subi duty_cycle,16; duty_cycle - 16
 minus2:
 in r16,pina;
 sbrs r16,pa3;
 rjmp minus2;
 rjmp next2;
