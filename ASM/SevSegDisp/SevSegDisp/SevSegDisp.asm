/*
 * SevSegDisp.asm
 *
 *  Created: 2015-04-30 23:04:45
 *   Author: SaddamAZR
 */ 

 .DEF counter1=r20; inicjalizacja licznikow i wartosci
 .DEF counter2=r21;
 .DEF counter3=r22;
 .EQU value1=248;
 .EQU value2=200;
 .EQU value3=107;

 .ORG 0x32;
 prime_cseg: .DB 0x7e,0x30,0x6d,0x79,0x33,0x5b,0x5f,0x70,0x7f,0x7b,0x77,0x1f,0x4e,0x3d,0x4f,0x47; zdekodowane wartosci 0-F dla wyswietlacza
 
 .DSEG
 .ORG 0x100;
 prime_dseg: .BYTE 16; rezerwacja miejsca w pamieci danych

 .CSEG
 .ORG 0x100; start pamieci programu
 start: 
 ldi r16,0xff;
 out ddrb,r16; PORTB - wyjscie (7SEG)
 out ddrd,r16; PORTD - wyjscie (7SEG DIGIT)
 ldi zh,high(2*prime_cseg); ladowanie do rejestru X adresu danych z prime:
 ldi zl,low(2*prime_cseg);
 ldi yh,high(prime_dseg);
 ldi yl,low(prime_dseg);
 ldi r17,16;
 store_to_dseg:; przepisanie zawartosci prime_cseg: do prime_dseg:
 lpm r16,z+;
 st y+,r16;
 dec r17;
 brne store_to_dseg;
 ldi r16,4;
 out portd,r16; wybranie 1 cyfry 7SEG

 reset:
 ldi yh,high(prime_dseg); reset wskaznika
 ldi yl,low(prime_dseg);
 program:
 ld r16,y+;
 com r16;
 out portb,r16; wyswietlanie na LED'ach
 com r16;
 call one_sec_delay;
 cpi r16,0x47;
 breq reset;
 rjmp program;

 one_sec_delay:; procedura opozniajaca
 loop1:
 ldi counter1,value1;
 ldi counter2,value2;
 ldi counter3,value3;
 loop2:
 dec counter1;
 brne loop2;
 ldi counter1,value1;
 dec counter2;
 brne loop2;
 ldi counter1,value1;
 ldi counter2,value2
 dec counter3;
 brne loop2
 ret

