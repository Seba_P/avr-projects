/*
 * SevSegDisp4.asm
 *
 *  Created: 2015-05-04 13:48:56
 *   Author: SaddamAZR
 */ 

.DEF counter1=r20;
.DEF counter2=r21;
.DEF counter_inc=r22;
.EQU value1=80;
.EQU value2=200;
.EQU value_inc=3;
.DEF reg_low=r23;
.DEF reg_high=r24;
.DEF disp_reg=r25;
.DEF addr_z=r19;
.DEF zero=r18;
.CSEG
.ORG 0x00 
rjmp start;

.ORG 0x32 
data_7seg: .DB 0x7e, 0x30, 0x6d, 0x79, 0x33, 0x5b, 0x5f, 0x70, 0x7f, 0x7b, 0x77, 0x1f, 0x4e, 0x3d, 0x4f, 0x47;  

.ORG 0x100 
start:
ldi r16,0xff;
out ddrb,r16; PORTB - wyjscie (dane na 7SEG)
out ddra,r16; PORTA - wyjscie (cyfra 7SEG)
ldi reg_high,0;
ldi reg_low,0; reg1,reg2 - rejestry licznika 16-bitowego
ldi addr_z,low(2*data_7seg); dolny bajt adresu data_7seg:
ldi zl,low(2*data_7seg);
ldi zh,high(2*data_7seg); inicjalizacja wskaznika Z
ldi zero,0;
ldi xl,1; xl - '1'

ldi r16,high(ramend); inicjalizacja stosu
out sph,r16;
ldi r16,low(ramend);
out spl,r16;

program:
clc; czyszczenie flagi przepelnienia
clh;
add reg_low,xl;
adc reg_high,zero;
ldi counter_inc,value_inc;
disp:
call display;
dec counter_inc;
brne disp;
rjmp program;

delay:; procedura opozniajaca (~50ms)
loop:
dec counter1;
brne loop;
ldi counter1,value1;
dec counter2;
brne loop;
ldi counter1,value1;
ldi counter2,value2;
ret

display:; procedura wyswietlania zawartosci licznika na 7SEG
; I cyfra
ldi r16,7;
out porta,r16;
mov disp_reg,reg_low;
andi disp_reg,0x0F;
add disp_reg,addr_z;
mov zl,disp_reg;
lpm r16,z;
com r16;
out portb,r16;
call delay;
; II cyfra
ldi r16,11;
out porta,r16;
mov disp_reg,reg_low;
andi disp_reg,0xF0;
swap disp_reg;
add disp_reg,addr_z;
mov zl,disp_reg;
lpm r16,z;
com r16;
out portb,r16;
call delay;
; III cyfra
ldi r16,13;
out porta,r16;
mov disp_reg,reg_high;
andi disp_reg,0x0F;
add disp_reg,addr_z;
mov zl,disp_reg;
lpm r16,z;
com r16;
out portb,r16;
call delay;
; IV cyfra
ldi r16,14;
out porta,r16;
mov disp_reg,reg_high;
andi disp_reg,0xF0;
swap disp_reg;
add disp_reg,addr_z;
mov zl,disp_reg;
lpm r16,z;
com r16;
out portb,r16;
call delay;
ret
