/*
 * C2F.asm
 *
 *  Created: 2015-05-17 13:09:48
 *   Author: SaddamAZR
 */ 

.DEF counter1=r20;
.DEF counter2=r21;
.EQU value1=80;
.EQU value2=200;
.DEF fahr_low=r23;
.DEF fahr_high=r24;
.DEF cels_low=r18;
.DEF cels_high=r19;
.DEF disp_reg=r25;
.DEF addr_z=r22;
.CSEG
.ORG 0x00 
rjmp start;

.ORG 0x32 
data_7seg: .DB 0x7e, 0x30, 0x6d, 0x79, 0x33, 0x5b, 0x5f, 0x70, 0x7f, 0x7b, 0x77, 0x1f, 0x4e, 0x3d, 0x4f, 0x47;  

.ORG 0x100 
start:
ldi r16,0xff;
out ddrb,r16; PORTB - wyjscie (dane na 7SEG)
out ddra,r16; PORTA - wyjscie (cyfra 7SEG)
out ddrd,r16; PORTD - wyjscie (diody dla flag C (bit 5), S (bit 6) i V (bit 7) oraz flaga bledu obliczen (bit 3) i znak liczby (bit 4))
out portd,r16; wstepne gaszenie diod
out portc,r16; ustawianie pull'upow dla switchy; PORTC - wejscie (switch do displaya)
ldi addr_z,low(2*data_7seg); dolny bajt adresu data_7seg:
ldi zl,low(2*data_7seg);
ldi zh,high(2*data_7seg); inicjalizacja wskaznika Z

ldi r16,high(ramend); inicjalizacja stosu
out sph,r16;
ldi r16,low(ramend);
out spl,r16;

program:
ldi fahr_high,0x80; zapis temperatury w stopniach Fahrenheita
ldi fahr_low,0x00;

asr fahr_high; pierwsza operacja - 1/2*fahr
brcc no_C_1;
lsr fahr_low;
brcc no_fail_1;
cbi portd,3; zapalenie diody dla flagi bledu obliczen 
no_fail_1:
ori fahr_low,0x80; przeniesienie bitu 0 z fahr_high do bitu 7 w fahr_low
rjmp next_1;
no_C_1:
lsr fahr_low;
next_1:
clc;
mov cels_high,fahr_high; przeniesienie do rejestru cels
mov cels_low,fahr_low;

asr fahr_high; druga operacja - 1/4*fahr
brcc no_C_2;
lsr fahr_low;
brcc no_fail_2;
cbi portd,3; zapalenie diody dla flagi bledu obliczen 
no_fail_2:
ori fahr_low,0x80; przeniesienie bitu 0 z fahr_high do bitu 7 w fahr_low
rjmp next_2;
no_C_2:
lsr fahr_low;
next_2:
clc;
asr fahr_high; pierwsza operacja - 1/8*fahr
brcc no_C_3;
lsr fahr_low;
brcc no_fail_3;
cbi portd,3; zapalenie diody dla flagi bledu obliczen 
no_fail_3:
ori fahr_low,0x80; przeniesienie bitu 0 z fahr_high do bitu 7 w fahr_low
rjmp next_3;
no_C_3:
lsr fahr_low;
next_3:
clc;
asr fahr_high; pierwsza operacja - 1/16*fahr
brcc no_C_4;
lsr fahr_low;
brcc no_fail_4;
cbi portd,3; zapalenie diody dla flagi bledu obliczen 
no_fail_4:
ori fahr_low,0x80; przeniesienie bitu 0 z fahr_high do bitu 7 w fahr_low
rjmp next_4;
no_C_4:
lsr fahr_low;
next_4:
clc;
clv;

add cels_low,fahr_low; dodanie wyniku - cels=5/9*fahr
adc cels_high,fahr_high;
clc;
clv;
clh;

ldi r16,-18;
add cels_low,r16; odjecie 5/9*32~~18
ldi r16,0xff;
adc cels_high,r16; gotowy wynik

brcc no_C_end;
cbi portd,5; dioda dla flagi C
no_C_end:
brge no_S_end;
cbi portd,6; dioda dla flagi S
no_S_end:
brvc disp;
cbi portd,7; dioda dla flagi V

disp:
sbic pinc,0; 
rjmp no_switch;
; display jako wartosc bezwzgledna + znak
push cels_high; ladowanie pierwotnych wartosci w U2 na stos
push cels_low;
subi cels_high,0; sprawdzanie znaku liczby przed negacja
brge plus;
cbi portd,4; ustawianie znaku na LED
neg cels_low; modul z liczby
com cels_high;
plus: 
rcall display;
sbi portd,4; zerowanie znaku przed kolejnym displayem
pop cels_low; przywracanie pierwotnych wartosci ze stosu
pop cels_high;
rjmp disp;
no_switch:; display w kodzie U2
rcall display;
rjmp disp;

delay:; procedura opozniajaca (~50ms)
loop:
dec counter1;
brne loop;
ldi counter1,value1;
dec counter2;
brne loop;
ldi counter1,value1;
ldi counter2,value2;
ret

display:; procedura wyswietlania zawartosci licznika na 7SEG
; I cyfra
ldi r16,7;
out porta,r16;
mov disp_reg,cels_low;
andi disp_reg,0x0F;
add disp_reg,addr_z;
mov zl,disp_reg;
lpm r16,z;
com r16;
out portb,r16;
rcall delay;
; II cyfra
ldi r16,11;
out porta,r16;
mov disp_reg,cels_low;
andi disp_reg,0xF0;
swap disp_reg;
add disp_reg,addr_z;
mov zl,disp_reg;
lpm r16,z;
com r16;
out portb,r16;
rcall delay;
; III cyfra
ldi r16,13;
out porta,r16;
mov disp_reg,cels_high;
andi disp_reg,0x0F;
add disp_reg,addr_z;
mov zl,disp_reg;
lpm r16,z;
com r16;
out portb,r16;
rcall delay;
; IV cyfra
ldi r16,14;
out porta,r16;
mov disp_reg,cels_high;
andi disp_reg,0xF0;
swap disp_reg;
add disp_reg,addr_z;
mov zl,disp_reg;
lpm r16,z;
com r16;
out portb,r16;
rcall delay;
ret

