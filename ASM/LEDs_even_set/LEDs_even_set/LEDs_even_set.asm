/*
 * LEDs_even_set.asm
 *
 *  Created: 2015-04-27 13:00:42
 *   Author: SaddamAZR
 */ 

 ldi r16,0xff;
 out ddra,r16; ustawianie PORTA jakos wyjscie
 
 sbi porta,1; ustawianie stanu LED'ow
 sbi porta,3;
 sbi porta,5;
 sbi porta,7;

 stop: rjmp stop;