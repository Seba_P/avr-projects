/*
 * one_sec_subroutine.asm
 *
 *  Created: 2015-04-27 15:08:15
 *   Author: SaddamAZR
 */ 

 .DEF counter1=r20; inicjalizacja licznikow i wartosci
 .DEF counter2=r21;
 .DEF counter3=r22;
 .EQU value1=248;
 .EQU value2=200;
 .EQU value3=107;
 .DEF led_state=r24; inicjalizacja rejestrow dla LED'ow
 .DEF led_display=r25;

 ldi r16,0xff;
 out ddrb,r16; PORTB - wyjscie (LED)
 ldi led_state,0x00;

 start:
 call one_sec_delay;
 inc led_state;
 mov led_display,led_state;
 com led_display;
 out portb,led_display; wyswietlanie LED'ow
 rjmp start;

 one_sec_delay:
 loop1:
 ldi counter1,value1;
 ldi counter2,value2;
 ldi counter3,value3;
 loop2:
 dec counter1;
 brne loop2;
 ldi counter1,value1;
 dec counter2;
 brne loop2;
 ldi counter1,value1;
 ldi counter2,value2
 dec counter3;
 brne loop2
 ret