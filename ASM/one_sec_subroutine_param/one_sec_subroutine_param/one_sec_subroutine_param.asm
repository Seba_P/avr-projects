/*
 * one_sec_subroutine_param.asm
 *
 *  Created: 2015-04-27 15:47:31
 *   Author: SaddamAZR
 */ 
 .DEF counter1=r20; deklarowanie licznikow i wartosci
 .DEF counter2=r21;
 .DEF counter3=r22;
 .DEF led_state=r24; deklarowanie rejestru dla LED'ow

 .ORG 0x00;
 .DSEG; rezerwacja pamieci danych
 value1: .BYTE 1
 value2: .BYTE 1
 value3: .BYTE 1

 .ORG 0x100;
 .CSEG; segment pamieci programu

 ldi r16,248; inicjalizacja parametrow
 sts value1,r16;
 ldi r16,200;
 sts value2,r16;
 ldi r16,107;
 sts value3,r16;
 lds counter1,value1; inicjalizacja licznikow
 lds counter2,value2;
 lds counter3,value3;
 
 ldi r16,high(RAMEND); deklaracja stosu
 out sph,r16;
 ldi r16,low(RAMEND);
 out spl,r16;

 ldi r16,0xff;
 out ddrb,r16; PORTB - wyjscie (LED)
 ldi led_state,0x00;

 start:
 call one_sec_delay;
 inc led_state;
 push led_state;
 com led_state;
 out portb,led_state; wyswietlanie LED'ow
 pop led_state;
 rjmp start;

 one_sec_delay:
 lds counter1,value1;
 lds counter2,value2;
 lds counter3,value3;
 loop2:
 dec counter1;
 brne loop2;
 lds counter1,value1;
 dec counter2;
 brne loop2;
 lds counter1,value1;
 lds counter2,value2;
 dec counter3;
 brne loop2
 ret
