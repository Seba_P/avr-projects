/*
 * buttons_2_port.asm
 *
 *  Created: 2015-04-27 13:17:06
 *   Author: SaddamAZR
 */ 

ldi r16,0xff;
out ddrb,r16; PORTB jakos wyjscie - LED'y
out portd,r16; ustawianie pull'upow dla PORTD - switche

start: out portb,r16; zerowanie stanu LED'ow
sbis pind, 0; switch wcisniety, stan='0'
cbi portb, 0; LED wlaczony, gdy stan pinu='0'
sbis pind, 1; 
cbi portb, 1;
sbis pind, 2;
cbi portb, 2;
sbis pind, 3;
cbi portb, 3;
sbis pind, 4;
cbi portb, 4;
sbis pind, 5;
cbi portb, 5;
sbis pind, 6;
cbi portb, 6;
sbis pind, 7;
cbi portb, 7;
rjmp start;
