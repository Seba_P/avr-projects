/*
 * timer0_fifth.asm
 *
 *  Created: 2015-06-05 11:53:53
 *   Author: SaddamAZR
 */ 

 ldi r16,0xff;
 out ddra,r16; PORTA - wyjscie (LED)
 out portb,r16; PORTB - wejscie (switch)
 out porta,r16; wstepne gaszenie diod
 in r16,tifr0;
 sbr r16,(1<<tov0);
 out tifr0,r16; zerowanie flagi TOV0
 ldi r16,7; 
 out tccr0b,r16; ustawienie zegara zewnetrznego dla TIMER0 (falling edge)
 ldi r16,0xfb; wstepne ustawienie timera
 out tcnt0,r16;

 program:
 in r16,tifr0;
 sbrc r16,tov0;
 cbi porta,0; zapalenie diody
 rjmp program;