/*
 * signed.asm
 *
 *  Created: 2015-05-13 18:01:25
 *   Author: SaddamAZR
 */ 

 ldi r20,70;
 ldi r21,96;
 add r20,r21; 70+96
 clc; czyszczenie flag w SREG
 clv;
 cln;
 cls;
 clh;
 ldi r20,-70;
 ldi r21,-96;
 add r20,r21; -70-96
 clc; czyszczenie flag w SREG
 clv;
 cln;
 cls;
 clh;
 ldi r20,-126;
 ldi r21,30;
 add r20,r21; -126+30
 clc; czyszczenie flag w SREG
 clv;
 cln;
 cls;
 clh;
 ldi r20,126;
 ldi r21,-6;
 add r20,r21; 126-6
 clc; czyszczenie flag w SREG
 clv;
 cln;
 cls;
 clh;
 ldi r20,-2;
 ldi r21,-5;
 add r20,r21; -2-5
 clc; czyszczenie flag w SREG
 clv;
 cln;
 cls;
 clh;

 stop: rjmp stop;