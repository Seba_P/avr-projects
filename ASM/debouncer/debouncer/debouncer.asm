/*
 * debouncer.asm
 *
 *  Created: 2015-06-11 11:34:44
 *   Author: SaddamAZR
 */ 
 .DEF counter=r20;

 .ORG 0x00;
 rjmp start;
 .ORG 0x20;
 rjmp ocf0a_int;
 
 .ORG 0x60;
 start:
 ldi r16,0xff;
 out ddrc,r16; PORTC - wyjscie (LED)
 ldi r16,(1<<pa2);
 out porta,r16; PA2 - wejscie (switch); podciagniete pull'upy
 ldi r16,(1<<cs00)|(1<<cs02);
 out tccr0b,r16; TIMER0 - 1/1024
 ldi r16,(1<<wgm01)|(1<<com0a0);
 out tccr0a,r16; TIMER0 - tryb CTC
 ldi r16,(16*5);
 out ocr0a,r16; ustawianie wartosci dla OCR0A (~5ms)
 ldi r16,(1<<ocie0a);
 sts timsk0,r16; wlaczenie przerwania dla TIMER0

 ldi r16,high(ramend); deklaracja stosu
 out sph,r16;
 ldi r16,low(ramend);
 out spl,r16;

 ldi counter,0xff; wstepne gaszenie diod
 out portc,counter;
 sei; wlaczenie przerwan
 program:
 rjmp program;

 ocf0a_int:
 cli; blokada przerwan
 in r17,pina;
 sbrc r17,pa2;
 rjmp end;
 pushed:
 in r17,pina;
 sbrs r17,pa2;
 rjmp pushed;
 dec counter; zwiekszenie licznika ("0" to stan wysoki dla diody)
 out portc,counter;
 end:
 sei; odblokowanie przerwan
 reti;