/*
 * keypad_int.asm
 *
 *  Created: 2015-06-13 12:19:10
 *   Author: SaddamAZR
 */ 
.DEF button_code=r20

.CSEG
.ORG 0;
rjmp start;
.ORG INT2addr;
rjmp keypad_ISR ;Keyoad External Interrupt Request 2

.ORG 0x60;
; Main program start
start:
; Set Stack Pointer to top of RAM
ldi r16,high(ramend);
out sph,r16;
ldi r16,low(ramend);
out spl,r16;
;Set up port A as output for LED controls
ldi r16,0xff;
out ddra,r16;
; Clear intf2 flag
ldi r16,(1<<intf2);
out gifr,r16;
; Enable Int2
ldi r16,(1<<int2);
out gicr,r16;
; Set Int2 active on falling edge
ldi r16,(0<<isc2);
out mcucr,r16;
;Set rows as inputs and columns as outputs
ldi r16,0x0f;
out ddrd,r16;
;Set rows to high (pull ups) and columns to low to activate JP13
swap r16;
out portd,r16;
;Global Enable Interrupt
sei;
;Set up infinite loop
loop:
;read button code from r20 and send to port A
out porta,button_code;
rjmp loop
;Keypad Interrupt Service Routine
keypad_ISR:
push r16;
push button_code;
;Disable interrupts
cli;
;Set rows as outputs and columns as inputs on Port D
ldi r17,0xf0;
out ddrd,r17;
;Set rows to low and columns to high (pull up) on Port D
swap r17;
out portd,r17;
;Read Port D. Column code in lower nibble
nop ;!!!!
in r16,pind; 
;Store column code to r20 on lower nibble
mov button_code,r16;
;Set rows as inputs and columns as outputs on Port D
out ddrd,r17;
;Set rows to high (pull up) and columns to low on Port D
swap r17;
out portd,r17;
;Read Port D. Row code in higher nibble
nop ;!!!!!
in r16,pind;
;Store row code to r20 on higher nibble
andi button_code,0x0f;
andi r16,0xf0;
or button_code,r16;
;Clear intf2 flag
ldi r16,(1<<intf2);
out gifr,r16;
;Restore registers from stack (in opposite order)
pop button_code;
pop r16;
;Enable interrupts globally
sei;
reti;
