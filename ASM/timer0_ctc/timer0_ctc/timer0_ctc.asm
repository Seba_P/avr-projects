/*
 * timer0_ctc.asm
 *
 *  Created: 2015-06-08 19:08:51
 *   Author: SaddamAZR
 */ 

 ldi r16,0xFF;
 sbi ddrb,3;
 out portb,r16; PORTB0 - wejscie (switch),PORTB3 - wyjscie (LED)
 ldi r16,7;
 out tccr0b,r16; TIMER0 - zewnetrzny zegar (rising edge)
 ldi r16,(1<<wgm01)|(1<<com0a0);
 out tccr0a,r16; TIMER0 - tryb CTC
 ldi r16,5;
 out ocr0a,r16; ustawianie wartosci dla OCR0A

 program:
 in r16,tifr0;
 rjmp program;


