/*
 * mod_cnt_jmp.asm
 *
 *  Created: 2015-04-27 15:01:54
 *   Author: SaddamAZR
 */ 

 .DEF counter1=r20; inicjalizacja licznikow
 .DEF counter2=r21;
 .DEF counter3=r22;
 .DEF counter4=r23;
 .DEF led_state=r24;
 .DEF led_display=r25; inicjalizacja wyswietlacza LED (bo LED swieci dla '0')
 .EQU value1=248; inicjalizacja wartosci licznikow
 .EQU value2=200;
 .EQU value3=105;
 .EQU value4=10;
 .EQU value_start=5;

 ldi r16,0xff;
 out ddrb,r16; PORTB - wyjscie (LED) 

 loop1: 
	ldi led_state,value_start;
	mov led_display,led_state;
	com led_display;
	out portb,led_display; 
	ldi counter1,value1;
	ldi counter2,value2;
	ldi counter3,value3;
	ldi counter4,value4;
 loop2: 
    dec counter1;
	brne loop2;
	ldi counter1,value1;
	dec counter2;
	brne loop2;
	ldi counter1,value1;
	ldi counter2,value2;
	dec counter3;
	brne loop2;
	ldi counter1,value1;
	ldi counter2,value2;
	ldi counter3,value3;
	inc led_state;
	mov led_display,led_state;
	com led_display;
	out portb,led_display; wyswietlanie LED'ow
	dec counter4;
	brne loop2;
	
	rjmp 1loop;
	1loop: jmp loop1;

